package radical.time.tracker;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.DialogInterface;
import android.content.Intent;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.widget.Button;

public class ApplicationPauseResumeTimeTest extends
        ActivityInstrumentationTestCase2<MainActivity> {

    private MainActivity mainActivity;
    MainActivity.TimeTrackerFragment placeHolder;
    TimeTracker timeTracker;
    private Instrumentation mInstrumentation;

    String logTag = "timeLog";

    public ApplicationPauseResumeTimeTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mainActivity = getActivity();
        placeHolder = (MainActivity.TimeTrackerFragment) mainActivity.getFragmentManager().findFragmentByTag("placeHolderTag");
        timeTracker = placeHolder.timeTracker;
        mInstrumentation = getInstrumentation();
    }

    /**
     * First test for all 0's in the fields
     */
    public void testOnPauseAndOnResume() throws Throwable {
        //get current time from timer thread text view
        String currentTimerText = timeTracker.timerText.getText().toString();
        Log.d(logTag, "timer text = " + currentTimerText);

        mInstrumentation.waitForIdleSync();
        if (timeTracker.testAlert == null) {


        } else {

            final Button alertButton = timeTracker.testAlert.getButton(DialogInterface.BUTTON_POSITIVE);
            assertNotNull(alertButton);

            performClick(alertButton);

            String afterAddingTimeTextString = timeTracker.timerText.getText().toString();
            Log.d(logTag, "timer text = " + afterAddingTimeTextString);

            assertTrue(!currentTimerText.equals(afterAddingTimeTextString));
        }
    }

    /**
     * First test for all 0's in the fields
     */
    public void testOnPauseAndOnResume2() throws Throwable {
        //get current time from timer thread text view
        String currentTimerText = timeTracker.timerText.getText().toString();
        Log.d(logTag, "timer text = " + currentTimerText);

        mInstrumentation.waitForIdleSync();

        final Button alertButton = timeTracker.testAlert.getButton(DialogInterface.BUTTON_POSITIVE);
        assertNotNull(alertButton);

        performClick(alertButton);

        String afterAddingTimeTextString = timeTracker.timerText.getText().toString();
        Log.d(logTag, "timer text = " + afterAddingTimeTextString);

        assertTrue(!currentTimerText.equals(afterAddingTimeTextString));

    }

    private void performClick(final Button button) throws Throwable {
        runTestOnUiThread(new Runnable() {
            public void run() {
                button.performClick();
            }
        });
        mInstrumentation.waitForIdleSync();
    }

    private void performPause(final MainActivity mainActivity) throws Throwable {
        runTestOnUiThread(new Runnable() {
            public void run() {
                mInstrumentation.callActivityOnPause(mainActivity);
            }
        });
        mInstrumentation.waitForIdleSync();
    }


    private void performOnStop(final MainActivity mainActivity) throws Throwable {
        runTestOnUiThread(new Runnable() {
            public void run() {
                mInstrumentation.callActivityOnStop(mainActivity);
            }
        });
        mInstrumentation.waitForIdleSync();
    }

    private void performOnRestart(final MainActivity mainActivity) throws Throwable {
        runTestOnUiThread(new Runnable() {
            public void run() {
                mInstrumentation.callActivityOnRestart(mainActivity);
            }
        });
        mInstrumentation.waitForIdleSync();
    }

    private void performOnStart(final MainActivity mainActivity) throws Throwable {
        runTestOnUiThread(new Runnable() {
            public void run() {
                mInstrumentation.callActivityOnStart(mainActivity);
            }
        });
        mInstrumentation.waitForIdleSync();
    }

    private void performResume(final MainActivity mainActivity) throws Throwable {
        runTestOnUiThread(new Runnable() {
            public void run() {
                mInstrumentation.callActivityOnResume(mainActivity);
            }
        });
        mInstrumentation.waitForIdleSync();
    }

    public void callActivityStart(final Activity activity) {
        getInstrumentation().runOnMainSync(new Runnable() {
            public void run() {
                activity.startActivity(new Intent(activity, MainActivity.class));
            }
        });
    }
}