package radical.time.tracker;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

public class ApplicationTest extends
        ActivityInstrumentationTestCase2<MainActivity> {

    private MainActivity mainActivity;
    MainActivity.TimeTrackerFragment placeHolder;
    TimeTracker timeTracker;

    String logTag = "testLog";

    public ApplicationTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mainActivity = getActivity();
        placeHolder = (MainActivity.TimeTrackerFragment) mainActivity.getFragmentManager().findFragmentByTag("placeHolderTag");
        timeTracker = placeHolder.timeTracker;
    }

    /**
     * First test for all 0's in the fields
     */
    public void testInitTimeZeroes() {

        int expectedSeconds = 0;
        int expectedMinutes = 0;
        int expectedHours = 0;
        int expectedDays = 0;

        int actualSeconds = timeTracker.seconds;
        int actualMinutes = timeTracker.minutes;
        int actualDays = timeTracker.days;
        int actualHours = timeTracker.hours;

        Log.d(logTag, "actual " + actualSeconds + " expected " + expectedSeconds);
        assertEquals(actualSeconds, expectedSeconds);
        Log.d(logTag, "actual " + actualMinutes + " expected " + expectedMinutes);
        assertEquals(actualMinutes, expectedMinutes);
        Log.d(logTag, "actual " + actualHours + " expected " + expectedHours);
        assertEquals(actualHours, expectedHours);
        Log.d(logTag, "actual " + actualDays + " expected " + expectedDays);
        assertEquals(actualDays, expectedDays);
    }

    /**
     *  Test that 60 seconds will carry over to one minute.
     */
    public void testSeconds(){
        int seconds = 60;

        timeTracker.seconds = seconds;
        timeTracker.straightenTime();

        int expectedSeconds = 0;
        int expectedMinutes = 1;
        int expectedHours = 0;
        int expectedDays = 0;

        int actualSeconds = timeTracker.seconds;
        int actualMinutes = timeTracker.minutes;
        int actualDays = timeTracker.days;
        int actualHours = timeTracker.hours;

        Log.d(logTag, "actual " + actualSeconds + " expected " + expectedSeconds);
        assertEquals(actualSeconds, expectedSeconds);
        Log.d(logTag, "actual " + actualMinutes + " expected " + expectedMinutes);
        assertEquals(actualMinutes, expectedMinutes);
        Log.d(logTag, "actual " + actualHours + " expected " + expectedHours);
        assertEquals(actualHours, expectedHours);
        Log.d(logTag, "actual " + actualDays + " expected " + expectedDays);
        assertEquals(actualDays, expectedDays);
    }

    /**
     * Test that 60 minutes will carry over to one hour
     */
    public void testMinutes(){
        int minutes = 60;

        timeTracker.minutes = minutes;
        timeTracker.straightenTime();

        int expectedSeconds = 0;
        int expectedMinutes = 0;
        int expectedHours = 1;
        int expectedDays = 0;

        int actualSeconds = timeTracker.seconds;
        int actualMinutes = timeTracker.minutes;
        int actualDays = timeTracker.days;
        int actualHours = timeTracker.hours;

        Log.d(logTag, "actual " + actualSeconds + " expected " + expectedSeconds);
        assertEquals(actualSeconds, expectedSeconds);
        Log.d(logTag, "actual " + actualMinutes + " expected " + expectedMinutes);
        assertEquals(actualMinutes, expectedMinutes);
        Log.d(logTag, "actual " + actualHours + " expected " + expectedHours);
        assertEquals(actualHours, expectedHours);
        Log.d(logTag, "actual " + actualDays + " expected " + expectedDays);
        assertEquals(actualDays, expectedDays);
    }

    /**
     * Test that 60 minutes will carry over to one hour
     */
    public void testHours(){
        int hours = 24;

        timeTracker.hours = hours;
        timeTracker.straightenTime();

        int expectedSeconds = 0;
        int expectedMinutes = 0;
        int expectedHours = 0;
        int expectedDays = 1;

        int actualSeconds = timeTracker.seconds;
        int actualMinutes = timeTracker.minutes;
        int actualDays = timeTracker.days;
        int actualHours = timeTracker.hours;

        Log.d(logTag, "actual " + actualSeconds + " expected " + expectedSeconds);
        assertEquals(actualSeconds, expectedSeconds);
        Log.d(logTag, "actual " + actualMinutes + " expected " + expectedMinutes);
        assertEquals(actualMinutes, expectedMinutes);
        Log.d(logTag, "actual " + actualHours + " expected " + expectedHours);
        assertEquals(actualHours, expectedHours);
        Log.d(logTag, "actual " + actualDays + " expected " + expectedDays);
        assertEquals(actualDays, expectedDays);
    }

    /**
     * Regardless of the second value at the start, what is left is the mod of
     * 60
     */
    public void testStraightenTime() {
        int max = 1000;
        int min = -1000;

        for (int i = min; i < max; i++) {
            System.out.println("i" + i);

            timeTracker.seconds = i;
            timeTracker.minutes = 0;
            timeTracker.hours = 0;
            timeTracker.days = 0;
            timeTracker.straightenTime();

            int expectedSeconds = i % 60;
            int minutesAdded = i / 60;
            int expectedMinutes = minutesAdded % 60;
            int hoursAdded = minutesAdded / 60;
            int expectedHours = (hoursAdded % 24);
            int daysAdded = hoursAdded / 24;
            int expectedDays = daysAdded;

            int actualSeconds = timeTracker.seconds;
            int actualMinutes = timeTracker.minutes;
            int actualDays = timeTracker.days;
            int actualHours = timeTracker.hours;

            Log.d(logTag, "actual " + actualSeconds + " expected " + expectedSeconds);
            assertEquals(actualSeconds, expectedSeconds);
            Log.d(logTag, "actual " + actualMinutes + " expected " + expectedMinutes);
            assertEquals(actualMinutes, expectedMinutes);
            Log.d(logTag, "actual " + actualHours + " expected " + expectedHours);
            assertEquals(actualHours, expectedHours);
            Log.d(logTag, "actual " + actualDays + " expected " + expectedDays);
            assertEquals(actualDays, expectedDays);
        }
    }
}