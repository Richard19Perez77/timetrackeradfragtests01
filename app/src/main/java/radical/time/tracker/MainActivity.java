package radical.time.tracker;

import android.app.Dialog;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

/**
 * A class to start the activity code.
 *
 * @author Rick
 */
public class MainActivity extends AppCompatActivity {

    /**
     * On create of the Activity we need to inflate the view when we set it as
     * content. We also check that the user's phone is using the current google
     * play store version so our admob can work.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        Integer resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        switch (resultCode) {
            case ConnectionResult.SUCCESS:
                // Do what you want
                break;
            default:
                Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode,
                        this, 0);
                if (dialog != null) {
                    // This dialog will help the user update to the latest
                    // GooglePlayServices
                    dialog.show();
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /**
     * A placeholder fragment containing a simple view. The TimeTracker class is
     * used to perform the logic needed to run the application.
     */
    public static class TimeTrackerFragment extends Fragment {

        public final TimeTracker timeTracker = new TimeTracker();

        /**
         * On create of the placeholder fragment we can reset values that the
         * user had in the text fields.
         */
        @Override
        public void onCreate(Bundle savedInstanceState) {
            // on create uses saved instance to restore a lost running activity
            // text input fields values
            super.onCreate(savedInstanceState);
            setRetainInstance(true);
            // Check whether we're recreating a previously destroyed instance
            if (savedInstanceState != null) {
                // Restore value of members from saved state
                timeTracker.setSecondsText(savedInstanceState
                        .getString("seconds"));
                timeTracker.setMinutesText(savedInstanceState
                        .getString("minutes"));
                timeTracker.setHoursText(savedInstanceState.getString("hours"));
                timeTracker.setDaysText(savedInstanceState.getString("days"));
            }
        }

        /**
         * We can create the view by inflating the layout and then using the
         * time tracker class to set up the values displayed to the user.
         */
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_my, container,
                    false);
            timeTracker.onCreate(rootView);
            return rootView;
        }

        /**
         * when resuming we want to have the user think the application is in
         * the state they left it. We can select the last slot they did. We can
         * also update the view of records for that slot.
         */
        @Override
        public void onResume() {
            super.onResume();
            timeTracker.onResume();
        }

        @Override
        public void onSaveInstanceState(Bundle bundle) {
            // save the values from the input fields
            super.onSaveInstanceState(bundle);
            bundle.putString(timeTracker.getSeconds(), "seconds");
            bundle.putString(timeTracker.getMinutes(), "minutes");
            bundle.putString(timeTracker.getHours(), "hours");
            bundle.putString(timeTracker.getDays(), "days");
        }

        @Override
        public void onPause() {
            super.onPause();
            timeTracker.onPause();
        }

        /**
         * On stop of the application the only item we need to track in our time
         * tracker is the timer thread that is running and if its supposed to be
         * running on resume.
         */
        @Override
        public void onStop() {
            super.onStop();
            timeTracker.onStop();
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            timeTracker.onDestroy();
        }
    }

    /**
     * We have to implement the ad view in a fragment, this will call the
     * request to build and show. Per the docs we resume, pause and destroy the
     * ad view as well with the Activity.
     *
     * @author Rick
     */
    public static class AdFragment extends Fragment {

        public AdView mAdView;

        @Override
        public void onActivityCreated(Bundle bundle) {
            super.onActivityCreated(bundle);
            View view = getView();
            if (view != null) {
                mAdView = (AdView) view.findViewById(R.id.adView);
                AdRequest adRequest = new AdRequest.Builder().build();
                mAdView.loadAd(adRequest);
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_ad, container, false);
        }

        @Override
        public void onResume() {
            super.onResume();

            // Resume the AdView.
            mAdView.resume();
        }

        @Override
        public void onPause() {
            // Pause the AdView.
            mAdView.pause();

            super.onPause();
        }

        @Override
        public void onDestroy() {
            // Destroy the AdView.
            mAdView.destroy();

            super.onDestroy();
        }
    }
}