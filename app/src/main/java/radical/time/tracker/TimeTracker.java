package radical.time.tracker;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.TransitionDrawable;
import android.os.IBinder;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Tracking time is done by accepting user input in the time fields and either
 * adding it to the logs or subtracting it from the logs. Time increments are
 * stored as all positive values for adding and all negative values for
 * subtracting. When rolling these up for the record view time is then simply
 * added or subtracted for the running total.
 *
 * @author Rick
 */
public class TimeTracker {

    /**
     * The user's records are printed out in detail in this view.
     */
    private WebView webview;

    /**
     * The slot buttons are different instances of time tracking.
     */
    private Button slot1Button;
    private Button slot2Button;

    /**
     * Allow for the user to give input when performing actions such as delete
     * that may have consequences.
     */
    public AlertDialog.Builder alert;
    public AlertDialog testAlert;

    /**
     * Creates a small window alert for notifications.
     */
    private Toast myToast;

    /**
     * Displays the current timer increment
     */
    public TextView timerText;

    /**
     * Static variables for database transactions
     */
    private static final String DATABASE_NAME = "timetrackerdatabase.db";
    private static final String TIME_TRACKER_TABLE = "time_records";
    private static final String COLUMN_ID = "ID";
    private static final String COLUMN_DAYS = "DAYS";
    private static final String COLUMN_HOURS = "HOURS";
    private static final String COLUMN_MINUTES = "MINUTES";
    private static final String COLUMN_SECONDS = "SECONDS";
    private static final String COLUMN_COMMENT = "COMMENT";
    private static final String COLUMN_NAME = "NAME";
    private static final String COLUMN_SLOT = "SLOT";

    private static final String DEFAULT_SLOT_ONE = "Tracking 1";
    private static final String DEFAULT_SLOT_TWO = "Tracking 2";

    /**
     * Used to pull field values and perform time calculations.
     */
    public int days;
    public int hours;
    public int minutes;
    public int seconds;

    private long currentTimeSpan = 0;

    private int currentSlot;

    private int previousSlot;

    private int records;

    private String slot1Name;
    private String slot2Name;
    private String currentSlotName;

    private SQLiteDatabase scoreDB;

    private EditText renameInput;

    private TimerThread timerThread;

    private Long timeToAdd = 0l;

    /**
     * The transition is the animation when pressing a button or on release of a
     * button press.
     */
    private TransitionDrawable trans1;
    private TransitionDrawable trans2;
    private TransitionDrawable transa;
    private TransitionDrawable transb;
    private TransitionDrawable timerTrans;
    private static final
    int TRANSITION_DURATION = 500;

    /**
     * Used to tell if the timer should be incrementing seconds.
     */
    boolean isTimerRunning;

    /**
     * The time is stored as the initial startDate and pause, time will increase even
     * if the application is closed.
     */
    private Date startDate;
    private Date updateDate;

    private SharedPreferences sharedPreferences;

    private int timerSecond;
    private int timerMinute;
    private int timerHour;
    private int timerDay;

    private TextView sText;
    private TextView mText;
    private TextView hText;
    private TextView dText;

    /**
     * On long press of the timer button we can send the time to the input
     * fields but we need to format and display it using this string.
     */
    private String timerTextString;

    /**
     * Shared preferences variables.
     */
    private static final String MY_PREFERENCES = "MyPrefs";
    private static final String TIMER_RUNNING = "timerrunning";
    private static final String CURRENT_TIME_SPAN = "currentTimeSpan";
    private static final String EDITTEXT_DAYS = "days";
    private static final String EDITTEXT_HOURS = "hours";
    private static final String EDITTEXT_MINUTES = "minutes";
    private static final String EDITTEXT_SECONDS = "seconds";
    private static final String CLOSING_TIME = "closingTime";

    /**
     * The view object will be needed to show toast messages in the Fragment.
     */
    View view;

    Date pauseDate;
    Date resumeDate;

    public void onCreate(View rootView) {

        //store the root view for updating the UI from our timer thread
        view = rootView;

        //reference views that hold time increments
        sText = (TextView) rootView.findViewById(R.id.secondsInputText);
        mText = (TextView) rootView.findViewById(R.id.minutesInputText);
        hText = (TextView) rootView.findViewById(R.id.hoursInputText);
        dText = (TextView) rootView.findViewById(R.id.daysInputText);

        currentSlot = 1;
        previousSlot = 1;

        slot1Name = DEFAULT_SLOT_ONE;
        slot2Name = DEFAULT_SLOT_TWO;

        webview = (WebView) rootView.findViewById(R.id.webView1);

        //makes web view background transparent
        webview.setBackgroundColor(0);

        timerText = (TextView) rootView.findViewById(R.id.timerText);

        Button timerButton = (Button) rootView.findViewById(R.id.timerButton);
        timerButton.setBackgroundResource(R.drawable.transitionbottombuttons);
        timerTrans = (TransitionDrawable) timerButton.getBackground();
        timerTrans.startTransition(TRANSITION_DURATION);
        timerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timerToggle();
                timerTrans.startTransition(TRANSITION_DURATION);
            }
        });
        timerButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                alert = new AlertDialog.Builder(view.getContext());
                alert.setTitle("Confirm");
                alert.setMessage("Send " + timerTextString
                        + " to input fields?");
                alert.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                sendTimerToInputFields();
                                timerTrans.startTransition(TRANSITION_DURATION);
                            }
                        });
                alert.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                            }
                        });
                alert.show();
                return false;
            }
        });

        // reference the delete button
        Button delAllButton = (Button) rootView.findViewById(R.id.delAllButton);

        // set the xml of the transition drawable
        delAllButton.setBackgroundResource(R.drawable.transitionbottombuttons);

        // reference the transition drawable to be manipulated
        TransitionDrawable transd = (TransitionDrawable) delAllButton.getBackground();
        transd.startTransition(TRANSITION_DURATION);
        delAllButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert = new AlertDialog.Builder(view.getContext());
                alert.setTitle("Confirm");
                alert.setMessage("Delete All Records for " + currentSlotName);
                alert.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                delRecords();
                                updateWebView();
                            }
                        });
                alert.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                            }
                        });
                alert.show();
            }
        });

        Button delRecordButton = (Button) rootView.findViewById(R.id.delRecordButton);
        delRecordButton
                .setBackgroundResource(R.drawable.transitionbottombuttons);

        TransitionDrawable transc = (TransitionDrawable) delRecordButton.getBackground();
        transc.startTransition(TRANSITION_DURATION);
        delRecordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRecordCount();
                if (records > 0) {
                    alert = new AlertDialog.Builder(view.getContext());
                    alert.setTitle("Confirm");
                    alert.setMessage("Delete Record #" + records + " for "
                            + currentSlotName);
                    alert.setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {
                                    deleteLastRecord();
                                    updateWebView();
                                }
                            });
                    alert.setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {
                                }
                            });
                    alert.show();
                } else {
                    // give toast message to enter a number
                    String text = "No records in current slot to delete.";
                    showToast(text);
                }
            }
        });

        /**
         Tracking action buttons.
         */
        Button addRecordButton = (Button) rootView.findViewById(R.id.addRecordButton);
        addRecordButton
                .setBackgroundResource(R.drawable.transitionbottombuttons);
        transa = (TransitionDrawable) addRecordButton.getBackground();
        transa.startTransition(TRANSITION_DURATION);
        addRecordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removePhoneKeypad();
                getTimeFromTextFields();
                straightenTime();
                storeAddTime();
                updateWebView();
                transa.startTransition(TRANSITION_DURATION);
            }
        });

        Button subRecordButton = (Button) rootView.findViewById(R.id.subRecordButton);
        subRecordButton
                .setBackgroundResource(R.drawable.transitionbottombuttons);
        transb = (TransitionDrawable) subRecordButton.getBackground();
        transb.startTransition(TRANSITION_DURATION);
        subRecordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removePhoneKeypad();
                getTimeFromTextFields();
                straightenTime();
                storeSubtractTime();
                updateWebView();
                transb.startTransition(TRANSITION_DURATION);
            }
        });

        slot1Button = (Button) rootView.findViewById(R.id.slotOneButton);
        slot1Button.setBackgroundResource(R.drawable.transitiontopbuttons);
        trans1 = (TransitionDrawable) slot1Button.getBackground();
        trans1.startTransition(TRANSITION_DURATION);
        slot1Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                removePhoneKeypad();
                previousSlot = currentSlot;
                currentSlot = 1;
                currentSlotName = slot1Name;
                updateWebView();

                if (currentSlot != previousSlot) {
                    switchPrevButtonTransDraw();
                    trans1.startTransition(TRANSITION_DURATION);
                }
            }
        });

        slot1Button.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View arg0) {
                alert = new AlertDialog.Builder(view.getContext());
                renameInput = new EditText(view.getContext());
                alert.setTitle(R.string.rename_string);
                alert.setMessage(R.string.type_new_and_ok);
                alert.setView(renameInput);
                alert.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                previousSlot = currentSlot;
                                currentSlot = 1;

                                currentSlotName = renameInput.getText()
                                        .toString();

                                currentSlotName = cleanInputString(currentSlotName);

                                slot1Name = currentSlotName;
                                slot1Button.setText(currentSlotName);
                                reNameSlot();
                                if (currentSlot != previousSlot) {
                                    switchPrevButtonTransDraw();
                                    trans1.startTransition(TRANSITION_DURATION);
                                }
                                updateWebView();
                            }

                        });
                alert.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                            }
                        });
                alert.show();
                return true;
            }
        });

        slot2Button = (Button) rootView.findViewById(R.id.slotTwoButton);
        slot2Button.setBackgroundResource(R.drawable.transitiontopbuttons);
        trans2 = (TransitionDrawable) slot2Button.getBackground();
        slot2Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                removePhoneKeypad();
                previousSlot = currentSlot;
                currentSlot = 2;
                currentSlotName = slot2Name;

                if (currentSlot != previousSlot) {
                    switchPrevButtonTransDraw();
                    trans2.startTransition(TRANSITION_DURATION);
                }
                updateWebView();
            }
        });
        slot2Button.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View arg0) {
                alert = new AlertDialog.Builder(view.getContext());
                renameInput = new EditText(view.getContext());
                alert.setTitle(R.string.rename_string);
                alert.setMessage(R.string.type_new_and_ok);
                alert.setView(renameInput);
                alert.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                previousSlot = currentSlot;
                                currentSlot = 2;
                                currentSlotName = renameInput.getText()
                                        .toString();

                                currentSlotName = cleanInputString(currentSlotName);

                                slot2Name = currentSlotName;
                                slot2Button.setText(currentSlotName);
                                reNameSlot();
                                if (currentSlot != previousSlot) {
                                    switchPrevButtonTransDraw();
                                    trans2.startTransition(TRANSITION_DURATION);
                                }
                                updateWebView();
                            }
                        });
                alert.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                            }
                        });
                alert.show();
                return true;
            }
        });

        sharedPreferences = view.getContext().getSharedPreferences(
                MY_PREFERENCES, Context.MODE_PRIVATE);

        if (sharedPreferences.contains(EDITTEXT_DAYS)) {
            setDaysText(sharedPreferences.getString(EDITTEXT_DAYS, ""));
        }

        if (sharedPreferences.contains(EDITTEXT_HOURS)) {
            setHoursText(sharedPreferences.getString(EDITTEXT_HOURS, ""));
        }

        if (sharedPreferences.contains(EDITTEXT_MINUTES)) {
            setMinutesText(sharedPreferences.getString(EDITTEXT_MINUTES, ""));
        }

        if (sharedPreferences.contains(EDITTEXT_SECONDS)) {
            setSecondsText(sharedPreferences.getString(EDITTEXT_SECONDS, ""));
        }

        if (sharedPreferences.contains(CURRENT_TIME_SPAN)) {
            currentTimeSpan = sharedPreferences.getLong(CURRENT_TIME_SPAN, 0);
        }

        if (sharedPreferences.contains(CLOSING_TIME)) {
            Long closingTime = sharedPreferences.getLong(CLOSING_TIME, 0);
            if (closingTime > 0) {
                pauseDate = new Date(closingTime);
            }
        }

        // check for previous timer is running if fail set to not running
        if (sharedPreferences.contains(TIMER_RUNNING)) {
            isTimerRunning = sharedPreferences
                    .getBoolean(TIMER_RUNNING, false);
            if (isTimerRunning) {
                // if timer should be running startDate a new thread
                resumeTimerThread();
            } else {
                updateTimerTextView();
            }
        } else {
            updateTimerTextView();
        }
    }

    public void onResume() {
        scoreDB = view.getContext().openOrCreateDatabase(
                DATABASE_NAME,
                SQLiteDatabase.CREATE_IF_NECESSARY, null);
        scoreDB.execSQL("CREATE TABLE IF NOT EXISTS " + TIME_TRACKER_TABLE
                + " (" + COLUMN_ID + " INTEGER PRIMARY KEY, " + COLUMN_SLOT
                + " INTEGER, " + COLUMN_DAYS + " INTEGER, " + COLUMN_HOURS
                + " INTEGER, " + COLUMN_MINUTES + " INTEGER, " + COLUMN_SECONDS
                + " INTEGER," + COLUMN_COMMENT + " VARCHAR," + COLUMN_NAME
                + " VARCHAR) ");
        updateButtonNames();
        updateWebView();

        resumeDate = new Date();

        if (pauseDate != null) {
            timeToAdd = resumeDate.getTime() - pauseDate.getTime();
            long second = (timeToAdd / 1000) % 60;
            long minute = (timeToAdd / (1000 * 60)) % 60;
            long hour = (timeToAdd / (1000 * 60 * 60)) % 24;
            String time = String.format("%02d:%02d:%02d", hour, minute, second);
            alert = new AlertDialog.Builder(view.getContext());
            alert.setTitle("Add App Closed Time");
            alert.setMessage("Do you want to add " + time + " to the timer?");
            alert.setPositiveButton("Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int whichButton) {
                            currentTimeSpan += timeToAdd;
                            timeToAdd = 0l;
                            updateTimerTextView();
                        }
                    });
            alert.setNegativeButton("No",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int whichButton) {
                            //do nothing, should run as usual.
                            //set flag so time isn't added on resume
                        }
                    });
            alert.create();
            testAlert = alert.show();
        }
    }

    /**
     * The input string shouldn't have any carriage returns or whitespace.
     *
     * @param currentSlotName the current selected slot
     * @return the stripped down version of the currentSlotName
     */
    private String cleanInputString(String currentSlotName) {
        // remove returns in string
        currentSlotName = currentSlotName.replaceAll("[\n\r]", "");
        // remove excess white space
        currentSlotName = currentSlotName.trim().replaceAll("\\s+", " ");
        return currentSlotName;
    }

    /**
     * when adding or subtracting a time instance we need to parse the values
     * and add a zero for any missing values. We use exception handling to do
     * this, the parse integer call should throw an error if empty and we try to
     * turn it into an Integer.
     */
    private void getTimeFromTextFields() {
        try {
            days = Integer.parseInt(dText.getText().toString());
        } catch (NumberFormatException nfe1) {
            days = 0;
        }

        try {
            hours = Integer.parseInt(hText.getText().toString());
        } catch (NumberFormatException nfe1) {
            hours = 0;
        }

        try {
            minutes = Integer.parseInt(mText.getText().toString());
        } catch (NumberFormatException nfe1) {
            minutes = 0;
        }

        try {
            seconds = Integer.parseInt(sText.getText().toString());
        } catch (NumberFormatException nfe1) {
            seconds = 0;
        }

    }

    /**
     * All subtraction actions are stored as negative values so the total time
     * will be accurate.
     */
    private void storeSubtractTime() {
        switch (currentSlot) {
            case 1:
                currentSlotName = slot1Name;
                break;
            case 2:
                currentSlotName = slot2Name;
                break;
        }

        if (days != 0 || hours != 0 || minutes != 0 || seconds != 0) {
            // add by subtraction this means in the data base the
            // subtraction records are all negative values for each
            // field
            days *= -1;
            hours *= -1;
            minutes *= -1;
            seconds *= -1;

            ContentValues values = new ContentValues();
            values.put(COLUMN_SLOT, currentSlot);
            values.put(COLUMN_DAYS, days);
            values.put(COLUMN_HOURS, hours);
            values.put(COLUMN_MINUTES, minutes);
            values.put(COLUMN_SECONDS, seconds);
            values.put(COLUMN_NAME, currentSlotName);
            scoreDB.insert(TIME_TRACKER_TABLE, null, values);

            // set back to all positive numbers on screen so they appear
            // as they should
            days *= -1;
            hours *= -1;
            minutes *= -1;
            seconds *= -1;

            if (days != 0)
                dText.setText(String.valueOf(days));
            else
                dText.setText("");

            if (hours != 0)
                hText.setText(String.valueOf(hours));
            else
                hText.setText("");

            if (minutes != 0)
                mText.setText(String.valueOf(minutes));
            else
                mText.setText("");

            if (seconds != 0)
                sText.setText(String.valueOf(seconds));
            else
                sText.setText("");

        } else {
            // give toast message to enter a number
            String text = "Please enter an hour, minute or second.";
            showToast(text);
        }

    }

    /**
     * All addition actions are stored as positive values so the total time will
     * be accurate.
     */
    private void storeAddTime() {
        switch (currentSlot) {
            case 1:
                currentSlotName = slot1Name;
                break;
            case 2:
                currentSlotName = slot2Name;
                break;
        }

        // there needs to be at lease one value to use
        if (days != 0 || hours != 0 || minutes != 0 || seconds != 0) {
            ContentValues values = new ContentValues();
            values.put(COLUMN_SLOT, currentSlot);
            values.put(COLUMN_DAYS, days);
            values.put(COLUMN_HOURS, hours);
            values.put(COLUMN_MINUTES, minutes);
            values.put(COLUMN_SECONDS, seconds);
            values.put(COLUMN_NAME, currentSlotName);
            scoreDB.insert(TIME_TRACKER_TABLE, null, values);
        } else {
            // give toast message to enter a number
            String text = "Please enter a day, hour, minute or second.";
            showToast(text);
        }

        if (days != 0)
            dText.setText(String.valueOf(days));
        else
            dText.setText("");

        if (hours != 0)
            hText.setText(String.valueOf(hours));
        else
            hText.setText("");

        if (minutes != 0)
            mText.setText(String.valueOf(minutes));
        else
            mText.setText("");

        if (seconds != 0)
            sText.setText(String.valueOf(seconds));
        else
            sText.setText("");
    }

    private void setRecordCount() {
        // sets the count of records from current slot
        Cursor c = scoreDB.query(TIME_TRACKER_TABLE, new String[]{"*"},
                COLUMN_SLOT + "=" + currentSlot, null, null, null, null, null);
        records = c.getCount();
        c.close();
    }

    private void showToast(String message) {
        if (myToast != null)
            myToast.cancel();
        myToast = Toast
                .makeText(view.getContext(), message, Toast.LENGTH_SHORT);
        myToast.setText(message);
        myToast.show();
    }

    /**
     * On switching save slots we have an in transition for the button pressed
     * but we need to know which button was last in so we can perform a
     * transition out action.
     */
    private void switchPrevButtonTransDraw() {
        switch (previousSlot) {
            case 1:
                trans1.reverseTransition(TRANSITION_DURATION);
                break;
            case 2:
                trans2.reverseTransition(TRANSITION_DURATION);
                break;
            default:
                break;
        }
        // only one transition check needed at a time
        previousSlot = currentSlot;
    }

    /**
     * Time can accumulate over in seconds, minutes and days. So we may need to
     * straighten it out. Even though some say time is circular.
     */
    public void straightenTime() {
        // distribute over or under seconds and minutes

        if (seconds >= 60 || seconds <= -60) {
            minutes += seconds / 60;
            seconds = seconds % 60;
        }

        if (minutes >= 60 || minutes <= -60) {
            hours += minutes / 60;
            minutes = minutes % 60;
        }

        if (hours >= 24 || hours <= -24) {
            days += hours / 24;
            hours = hours % 24;
        }
    }

    /**
     * Long press on a save slot allows for it to be renamed.
     */
    private void reNameSlot() {
        ContentValues args = new ContentValues();
        if (currentSlotName.length() >= 10)
            currentSlotName = currentSlotName.substring(0, 10);
        args.put(COLUMN_NAME, currentSlotName);
        scoreDB.update(TIME_TRACKER_TABLE, args, COLUMN_SLOT + "="
                + currentSlot, null);
    }

    /**
     * Handler for button press to reset save slot.
     */
    private void resetCurrentSlotName() {
        // update current slot button name
        switch (currentSlot) {
            case 1:
                currentSlotName = slot1Name = DEFAULT_SLOT_ONE;
                slot1Button.setText(slot1Name);
                break;
            case 2:
                currentSlotName = slot2Name = DEFAULT_SLOT_TWO;
                slot2Button.setText(slot2Name);
                break;
        }
    }

    private void deleteLastRecord() {
        // gets the last record and removes it
        Cursor c = scoreDB.query(TIME_TRACKER_TABLE, new String[]{"*"},
                COLUMN_SLOT + "=" + currentSlot, null, null, null, null, null);
        // c.moveToFirst is the first record in the list
        c.moveToLast();
        if (c.getCount() > 0)
            scoreDB.delete(TIME_TRACKER_TABLE,
                    COLUMN_ID + "=" + c.getString(0), null);
        c.close();
    }

    private void delRecords() {
        days = hours = minutes = seconds = 0;
        dText.setText("");
        hText.setText("");
        mText.setText("");
        sText.setText("");
        resetCurrentSlotName();
        scoreDB.delete(TIME_TRACKER_TABLE, COLUMN_SLOT + "=" + currentSlot,
                null);
    }

    /**
     * Display the records associated with the current slot selected.
     */
    private void updateWebView() {
        // webview.clearView(); is deprecated
        webview.loadUrl("about:blank");

        Cursor cursor = scoreDB.query(TIME_TRACKER_TABLE, new String[]{"*"},
                COLUMN_SLOT + "=" + currentSlot, null, null, null, null, null);

        StringBuilder builder = new StringBuilder();
        builder.append("<table width='100%25'>");
        builder.append("<tr><td><h4>");
        builder.append("<FONT COLOR=00CC00># Name</FONT>");
        builder.append("</h4></td>");
        builder.append("<td><h4>");
        builder.append("<FONT COLOR=00CC00>Days</FONT>");
        builder.append("</h4></td>");
        builder.append("<td><h4>");
        builder.append("<FONT COLOR=00CC00>Hrs.</FONT>");
        builder.append("</h4></td>");
        builder.append("<td><h4>");
        builder.append("<FONT COLOR=00CC00>Min.</FONT>");
        builder.append("</h4></td>");
        builder.append("<td><h4>");
        builder.append("<FONT COLOR=00CC00>Sec.</FONT>");
        builder.append("</h4></td></tr>");

        cursor.moveToLast();

        days = hours = minutes = seconds = 0;

        int acc = 0;
        for (int i = cursor.getCount() - 1; i >= 0; i--) {
            builder.append("<tr>");
            builder.append("<td><h3><FONT COLOR=FFFFFF>").append(cursor.getCount() - acc).append(" ").append(cursor.getString(7)).append("</FONT></h3></td>");
            builder.append("<td><h3><FONT COLOR=FFFFFF>");
            days += Integer.parseInt(cursor.getString(2));
            builder.append(cursor.getString(2));
            builder.append("</FONT></h3></td>");
            builder.append("<td><h3><FONT COLOR=FFFFFF>");
            hours += Integer.parseInt(cursor.getString(3));
            builder.append(cursor.getString(3));
            builder.append("</FONT></h3></td>");
            builder.append("<td><h3><FONT COLOR=FFFFFF>");
            minutes += Integer.parseInt(cursor.getString(4));
            builder.append(cursor.getString(4));
            builder.append("</FONT></h3></td>");
            builder.append("<td><h3><FONT COLOR=FFFFFF>");
            seconds += Integer.parseInt(cursor.getString(5));
            builder.append(cursor.getString(5));
            builder.append("</FONT></h3></td>");
            builder.append("</tr>");
            acc++;
            cursor.moveToPrevious();
        }
        builder.append("</table></body></html>");

        cursor.close();
        straightenTime();

        builder.insert(
                0,
                "<html><body><h3><FONT COLOR=00CC00><center>Scrollable Records"
                        + "</center></FONT></h3><h4><FONT COLOR=FFFFFF><center>"
                        + currentSlotName
                        + "</FONT COLOR=FFFFFF><FONT COLOR=00CC00>: " + days
                        + " days " + hours + " hrs. " + minutes + " min. "
                        + seconds + " sec.</center></FONT></h4>");

        webview.loadData(builder.toString(), "text/html", "UTF-8");
        days = hours = minutes = seconds = 0;

    }

    /**
     * Restore user modified save slot names.
     */
    private void updateButtonNames() {
        // record an instance of each named slot
        Cursor c = scoreDB.query(TIME_TRACKER_TABLE, new String[]{"*"},
                null, null, null, null, null, null);
        c.moveToLast();
        ArrayList<Integer> numbers = new ArrayList<>();

        for (int i = c.getCount() - 1; i >= 0; i--) {
            int sl = Integer.parseInt(c.getString(1));
            if (!numbers.contains(sl)) {
                numbers.add(sl);
                String nm = c.getString(7);
                setSlotName(sl, nm);
            }
            c.moveToPrevious();
        }

        c.close();

        switch (currentSlot) {
            case 1:
                currentSlotName = slot1Name;
                break;
            case 2:
                currentSlotName = slot2Name;
                break;
        }
    }

    private void setSlotName(int slotInt, String slotString) {
        switch (slotInt) {
            case 1:
                slot1Name = slotString;
                slot1Button.setText(slot1Name);
                break;
            case 2:
                slot2Name = slotString;
                slot2Button.setText(slot2Name);
                break;
        }
    }

    private void removePhoneKeypad() {
        InputMethodManager inputManager = (InputMethodManager) view
                .getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        IBinder binder = view.getWindowToken();
        inputManager.hideSoftInputFromWindow(binder,
                InputMethodManager.HIDE_NOT_ALWAYS);

    }

    private void resumeTimerThread() {
        if (timerThread == null
                || timerThread.getState() == Thread.State.TERMINATED) {
            startDate = new Date();
            timerThread = new TimerThread(this);
            timerThread.start();
            isTimerRunning = true;
        }
    }

    private void startNewTimerThread() {
        if (timerThread == null
                || timerThread.getState() == Thread.State.TERMINATED) {
            startDate = new Date();
            currentTimeSpan = 0;
            timerThread = new TimerThread(this);
            timerThread.start();
            isTimerRunning = true;
        }
    }

    private void timerToggle() {
        if (!isTimerRunning) {

            //startDate new time span and new timer thread to update it
            if (startDate == null && updateDate == null) {

                //check starting new or resuming timer
                if (currentTimeSpan == 0) {
                    startNewTimerThread();
                    showToast("Timer started");
                } else {
                    resumeTimerThread();
                    showToast("Timer resumed");
                }
            } else {

                // give the user the option to resume or restart the timer
                alert = new AlertDialog.Builder(view.getContext());
                alert.setTitle("\uD83D\uDD50" + " TimeTracker");
                alert.setMessage("Resume or Restart the timer?");
                alert.setNegativeButton("Resume Timer",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                resumeTimerThread();
                                showToast("Timer resumed");
                            }
                        });
                alert.setPositiveButton("Restart Timer",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                // call method to parse values and add to edit
                                // text fields
                                startNewTimerThread();
                                showToast("Timer re-started");
                            }
                        });
                alert.setCancelable(true);
                alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        showToast("Nevermind!");
                    }
                });
                alert.show();
            }
        } else {

            //pause timer if its running
            pauseTimerThread();

            //update current time span
            updateCurrentTimeSpan();

            //update UI
            updateTimerTextView();
            showToast("Timer paused");
        }
    }

    private void updateCurrentTimeSpan() {

        // most recent timer start time
        long startMillis = startDate.getTime();

        // most recent timer end time
        long endMillis = updateDate.getTime();

        // add this time span to the running total
        long latestTimeSpan = endMillis - startMillis;
        currentTimeSpan = currentTimeSpan + latestTimeSpan;
    }

    /**
     * Update the time span from the running thread, called every second if the thread is running.
     */
    public void updateRunningTimeSpan() {

        //store the started time
        long startMillis = startDate.getTime();

        //reset the start date
        startDate = new Date();

        // get current time
        long currentMillis = startDate.getTime();

        // find time span and add to running total
        long latestCurrentTimeSpan = currentMillis - startMillis;
        currentTimeSpan += latestCurrentTimeSpan;

        //update the UI
        updateTimerTextView();
    }

    private void updateTimerTextView() {
        long seconds = TimeUnit.SECONDS
                .convert(currentTimeSpan, TimeUnit.MILLISECONDS);

        long minutes = seconds / 60;
        seconds = seconds % 60;

        long hours = minutes / 60;
        minutes = minutes % 60;

        long days = hours / 24;
        hours = hours % 60;

        timerSecond = (int) seconds;
        timerMinute = (int) minutes;
        timerHour = (int) hours;
        timerDay = (int) days;
        timerTextString = timerDay + "d." + timerHour + "h.\n" + timerMinute
                + "m." + timerSecond + "s";
        timerText.setText(timerTextString);
    }

    private void sendTimerToInputFields() {
        // get difference of pause from startDate time and print values in edit texts
        dText.setText("" + timerDay);
        sText.setText("" + timerSecond);
        mText.setText("" + timerMinute);
        hText.setText("" + timerHour);

        // fill in hint for values above timer amount
        if (timerDay == 0) {
            dText.setText("");
            if (timerHour == 0) {
                hText.setText("");
                if (timerMinute == 0) {
                    mText.setText("");
                }
            }
        }
    }

    private void pauseTimerThread() {
        if (timerThread != null && timerThread.isAlive()) {
            updateDate = new Date();
            timerThread.interrupt();
            timerThread = null;
            isTimerRunning = false;
        }
    }

    public void setSecondsText(String seconds) {
        // set string from seconds to input box
        if (sText != null)
            sText.setText(seconds);
    }

    public void setMinutesText(String minutes) {
        // set string from minutes to input box
        if (mText != null)
            mText.setText(minutes);
    }

    public void setHoursText(String hours) {
        // set string from hours to input box
        if (hText != null)
            hText.setText(hours);
    }

    public void setDaysText(String days) {
        // set string from days to input box
        if (dText != null)
            dText.setText(days);
    }

    public String getSeconds() {
        // get seconds for saving instance
        return sText.getText().toString();
    }

    public String getMinutes() {
        // get seconds for saving instance
        return mText.getText().toString();
    }

    public String getHours() {
        // get seconds for saving instance
        return hText.getText().toString();
    }

    public String getDays() {
        // get seconds for saving instance
        return dText.getText().toString();
    }

    public void onPause() {
        pauseDate = new Date();
    }

    public void onStop() {
        Editor editor = sharedPreferences.edit();
        editor.putBoolean(TIMER_RUNNING, isTimerRunning);
        if (isTimerRunning) {
            pauseTimerThread();
        }

        Long time = new Date().getTime();
        editor.putLong(CLOSING_TIME, time);
        editor.putLong(CURRENT_TIME_SPAN, currentTimeSpan);
        editor.putString(EDITTEXT_DAYS, getDays());
        editor.putString(EDITTEXT_HOURS, getHours());
        editor.putString(EDITTEXT_MINUTES, getMinutes());
        editor.putString(EDITTEXT_SECONDS, getSeconds());
        editor.apply();
    }

    public void onDestroy() {
        if (scoreDB.isOpen()) {
            scoreDB.close();
        }
        pauseTimerThread();
    }
}