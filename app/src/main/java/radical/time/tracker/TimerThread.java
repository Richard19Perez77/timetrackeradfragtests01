package radical.time.tracker;

import android.app.Activity;

/**
 * 
 * A class to hold the thread and logic for the timer object for a given task.
 * The timer currently is not tied to a particular save slot.
 * 
 * @author Rick
 *
 */
class TimerThread extends Thread {

	/**
	 * we need a reference to the view object to be able to update the timer's
	 * time to the user.
	 */
	private final TimeTracker timeTracker;

	public TimerThread(TimeTracker time) {
		// set instance of time tracker
		timeTracker = time;
	}

	/**
	 * While the thread is running we can see that sleep is called every second
	 * and when it wakes up we update the view and sleep for another second.
	 * The view must be updated on the UI thread.
	 */
	@Override
	public void run() {
		while (timeTracker.isTimerRunning) {
			Activity act = (Activity) timeTracker.view.getContext();
			act.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					timeTracker.updateRunningTimeSpan();
				}
			});

			if (timeTracker.isTimerRunning) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
}